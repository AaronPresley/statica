<html>
    <head>
        <title><?= $site->website_name ?> - <?= $page->title ?></title>

        <meta name="description" content="A static website CMS" />
        <meta name="keywords" content="static, website, CMS, wordpress, alternative, awesome, statica" />

        <meta name="author" content="Aaron Presley" />
        <meta name="DC.title" content="A static website CMS" />
        <meta name="DC.creator" content="Aaron Presley" />

        <meta name="Copyright" content="Copyright Aaron Presley. All Rights Reserved." />

        <link href="<?= $site->base_url ?><?= $site->template_root ?>css/bootstrap.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="offset1 span10">
                    <div class="row" style="margin-top:20px;">
                        <div class="span4">
                            <h1><a href="<?= $site->base_url ?>"><?= $site->website_name ?></a> <small><?= $site->website_desc ?></small></h1>
                        </div>
                        <div class="span6 pull-right">
                            <ul class="nav nav-pills" style="float:right;">
                                <?= list_pages() ?>
                            </ul>
                        </div>
                    </div>
