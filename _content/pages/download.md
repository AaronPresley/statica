----
TITLE:Download
ORDER:3
STATUS:live
----

## How Do I Get It?

<div class="row">
    <div class="offset3 span6">
        <div class="well">
            <h3>It's Free!</h3>
            <p>Statica is very much in development and is presented as-is. Fee free to work on it and improve as you wish.</p>
            <p style="margin-top:15px;"><center><a href="https://github.com/AaronPresley/Statica" class="btn-primary btn-large">Download From GitHub</a></center></p>

            <h3>Or...</h3>
            <p>Clone the repo using git:</p>
            <center><code>
                git clone git://github.com/AaronPresley/Statica.git
            </code></center>
        </div>
    </div>
</div>
