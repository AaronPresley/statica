----
TITLE:Nested Page
STATUS:live
----

## Oh cool!
You can do nested pages on here, too! Here's a picture of a cat.

<center><img src="<?= $this->site_data->media_root ?>images/tophat.jpg" /></center>
