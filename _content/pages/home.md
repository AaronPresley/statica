----
TITLE:Home
ORDER:1
STATUS:live
----


<div class="row">
	<div class="span6">
		<h2>What is it?</h2>
		<p><em>Statica</em> is a simple, static file CMS that uses <a href="http://daringfireball.net/projects/markdown/" target="_blank">Markdown</a> to style text. It's intended for people who don't like messing with Wordpress databases and what-not.</p>
		<p>You can also use HTML in your content. It's your world.</p>

		<h2>What can it do?</h2>
		<p><em>Statica</em> reads the content of your static website content and creates the navigation. It supports nested pages, too.</p>
	</div>
	<div class="span4">
		<div class="well">
			<h3 style="margin-bottom:5px;">Why should I use it?</h3>
			<ul>
				<li>You want a simple website CMS, but don't really want to mess with a whole Database</li>
				<li>You're okay with an in-development project.</li>
				<li>You like the idea of syncing your site's content with a Dropbox folder. You could update the file, hit save and it's immediately on your website.</li>
			</ul>
		</div>
	</div>
</div>
