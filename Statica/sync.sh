#!/bin/bash

site_content="/Users/apresley/Sites/_personal/statica/_content/"
source_content="/Users/apresley/Dropbox/Web Contents/statica_content/"

echo "Removing old content"
rm -rf $site_content*

echo "Copying new content over"
cp -R "${source_content}" "${site_content}"
