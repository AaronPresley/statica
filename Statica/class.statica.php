<?php
require_once( 'markdown.php' );

class Statica{
    // -------------- OUR VARIABLES--------------
    private $segments = NULL;
    private $page_data = array();
    public $site_data = NULL;

    private $file_ignore_pattern = array(
        "^\\.",
        '^_'
    );

    public function __construct(){$this->set_config();}

    // -------------- ALL PUBLIC FUNCTIONS --------------

    /*
     * The main function called from the index page 
     */
    public function init_site(){
        // Set the necessary vars
        $this->set_segments();

        // Determine which content file we're looking at
        $file = $this->return_file_to_process();

        // Show the page
        $this->show_page( $file );
    }


    /*
     *  Returns an array of all pages and their data
     */
    public function return_pages( $location = NULL, $recur = FALSE, $incl_content = TRUE ){
        if( $location === NULL )
            $location = './' . $this->site_data->content_root . 'pages/';

        if( $handle = opendir($location) ){
            $count = 0;
            $data = array();
            while( false !== ($file = readdir($handle)) ){
                $do_show = true;
                foreach( $this->file_ignore_pattern as $pattern ){
                    if( $do_show )
                        $do_show = !preg_match("/".$pattern."/i", $file);
                }

                if( $do_show ){
                    $file = $location . $file;

                    // This file is a directory
                    if( is_dir($file) ){
                        $file .= '/';
                        $dir = $this->return_dir_data($file);

                        $data[number_format($dir['order'], 0).'_'.$count]['parent'] = $dir;
                        $data[number_format($dir['order'], 0).'_'.$count][] = $this->return_pages($file, TRUE, $incl_content);

                    // This file is just a regular page
                    } else {
                        $page = $this->return_page_data($file, $incl_content);
                        if(
                            (strtolower($page->status) == 'live') &&
                            (!isset($page->in_menu) || strtolower($page->in_menu) != 'false')
                        ){
                            $data[number_format($page->order, 0).'_'.$count] = $page;
                        }
                    }
                    $count++;
                }
            }
        }

        ksort($data);
        return $data;
    }


    // -------------- ALL PRIVATE FUNCTIONS --------------

    /*
     *  Called from the init_site function. Analyizes current segments
     *  and determines which file should currently be processed.
     */
    private function return_file_to_process(){
        if( $this->segments === NULL )
            exit( "You must run set_segments() before return_file_to_process()" );

        // Set the base file
        $file = $this->site_data->content_root;


        // Are we looking at a blog or a page?
        if( current($this->segments) !== $this->site_data->post_segment ){
            $file .= 'pages/';
        } else {
            $file .= 'posts/';
        }

        // Add the last segment or the default page if empty
        if( count($this->segments) <= 1 ){
            $temp_segs = trim(end($this->segments));
            if( $temp_segs == "" )
                $temp_segs = $this->site_data->default_page;

        } else {
            $temp_segs = implode('/', $this->segments);
        }
        $file .= $temp_segs;


        // Add the file extension
        $file .= $this->site_data->content_ext;

        return $file;
    }


    /*
     *  Looks at the passed file and returns all of the meta data within
     */
    private function return_dir_data( $file = NULL ){
        if( $file === NULL || !is_dir($file) ){
            return FALSE; exit;
        } else if( !@fopen($file . '/_info', 'r') ){
            return FALSE; exit;
        }

        $raw_contents = file_get_contents($file . '/_info');
        $meta = $this->return_meta($raw_contents);
        unset( $meta->file_contents );
        $data = $meta->meta;

        $file_name = substr($file, 0, strlen($file)-1);
        $file_name = end(explode('/', $file_name));
        $file_name = current(explode('.',$file_name));
        
        $data['file_name'] = $file_name;
        $data['permalink'] = $file;

        return $data;
    }


    /*
     *  Analizes the raw contents of a file and splits up all
     *  the meta info  
     */
    public function return_meta( $raw_contents ){
        // A list of all of the required meta fields
        $required_meta = array(
            'title',
            'status'
        );

        // Don't allow files that don't have a META section
        if( strpos($raw_contents, '----') === FALSE )
            exit( "That file $file is missing the META section" );

        // Pull out the meta section from the contents
        preg_match('/----(.|\n*)+----/', $raw_contents, $matches);
        $raw_meta = $matches[0];

        // Process the meta section and split into arrays
        $meta = explode("\n", $raw_meta);
        $temp = array();
        foreach( $meta as $item ){
            if( strpos(strtolower($item), '----') === FALSE ){
                $thing = explode(':', $item);
                $temp[ strtolower(trim($thing[0])) ] = trim($thing[1]);
            }
        }
        $meta = $temp;

        // Make sure this file has all required meta
        $missing = array();
        foreach( $required_meta as $item ){
            if( !array_key_exists($item, $meta) )
                $missing[] = $item;
        }

        // Meta items were missing, show error
        if( count($missing) > 0 ){
            $message = "The required meta tags were missing: " . implode(', ', $missing) . '.<br/>';
            return $message;

        // All items included. Continue
        } else {
            // Get the file contents without the META block
            $file_contents = trim(substr($raw_contents, strlen($raw_meta)));

            // Set the return info
            $to_return['meta'] = $meta;
            $to_return['file_contents'] = $file_contents;
            
            // Do that return
            return (object)$to_return;
        }
    }


    /*
     *  Looks at the passed file and returns all necessary content
     */
    private function return_page_data( $file = NULL, $incl_content = TRUE ){
        // Make sure something was passed
        if( $file === NULL ){
            return FALSE; exit;
        }

        $file_contents = NULL;

        // Change the file path if we need to show the 404
        if( !@fopen($file, 'r') )
            $file = './' . $this->site_data->content_root . $this->site_data->error_contents['404'] . $this->site_data->content_ext;

        // Pull all of the text from the file
        $site = $this->site_data;

        ob_start();
        include($file);
        $raw_contents = ob_get_contents();
        ob_end_clean();

        // Return all of the file contents
        $the_meta = $this->return_meta( $raw_contents );
        // There was a problem with this file's meta
        if( !is_object($the_meta) ){
            echo $file . ' (' . $the_meta . ')';
        
        // We're good!
        } else {
            $meta = $the_meta->meta;
            $file_contents = $the_meta->file_contents;
            
            // Get the file name without the dir
            $file_name = end(explode('/', $file));
            $file_name = current(explode('.',$file_name));

            // Add all meta data as page data
            foreach( $meta as $key => $val ){ $data[$key] = $val; }
            $data = (object)$data;

            // Some other meta data
            $data->file_name = $file_name;
            $data->permalink = $this->site_data->base_url;
            $data->permalink .= substr($file, strpos($file, 'pages/')+strlen('pages/'));
            $data->permalink = substr($data->permalink, 0, strlen($data->permalink)-strlen($this->site_data->content_ext));


            if( $incl_content ){
                $data->content = Markdown($file_contents);
            }

            // Return that shiii
            return $data;
        }
    }


    /*
     *  This function runs the config php file and
     *  saves all of the array keys as variables in our
     *  class
     */
    private function set_config(){
        // Make sure the config file exists
        if( !@fopen(dirname(__FILE__). './../config.php', 'r') )
            exit( "The config file can't be found. Make sure it's in the root directory of the site." );

        // Read config file
        include( dirname(__FILE__) . './../config.php' );

        // Loop through each config item and set it
        // as a class variable
        if( $config ){
            foreach( $config as $key => $val){
                $this->site_data[$key] = $val;
            }
            $this->site_data = (object)$this->site_data;
        }
    }


    /*
     *  Does the actual display of content
     */
    public function show_page( $file = NULL, $template_name = NULL ){
        // Make sure our template directory actually exists
        if( !@fopen( './' . $this->site_data->template_root, 'r') )
            exit( "That template directory can't be found." );

        // Make sure a file was passed
        if( $file === NULL && $template_name === NULL )
            exit( "You either have to pass a \$file or a \$template parameter." );

        if( $template_name === NULL ){
            // This file is a post
            if( current($this->segments) == $this->site_data->post_segment ) {
                $template_name = 'post';

            } else if( $file === NULL || !@fopen($file, 'r') ) {
                $file = './' . $this->site_data->content_root . $this->site_data->error_contents['404'] . $this->site_data->content_ext;
                $template_name = '404';

            } else {
                $template_name = 'page';
            }

        } else {
            $this->set_segments();
            $file = $this->return_file_to_process();
        }

        // Set all of our variables to be used in the template
        $site = $this->site_data;
        $page = $this->return_page_data( $file );

        // Make sure this page is live
        if( $template === NULL && strtolower($page->status) != 'live' ){
            $template_name = 'private';
        }

        // Set the template file
        $template_dir = './' . $this->site_data->template_root . $template_name . '.php';

        // Make sure this template file exists
        if( !@fopen($template_dir, 'r') )
            exit( "The template file $template_dir can't be found." );

        // Do the actual showing of the thing
        ob_start();
        include($template_dir);
        $html = ob_get_contents();
        ob_end_clean();

        echo $html;
    }


    /*
     *  Reads all of the current $_GET params, cleans them up
     *  and sets them for the global var
     */
    private function set_segments(){
        $segments = explode('/', current(array_keys($_GET)));
        $cleaned_segs = array();

        // Loop through each segment
        foreach( $segments as $seg ){
            // Change all underlines and spaces to hyphens
            $seg = preg_replace('/[[:space:]]/', '-', trim($seg));

            // Make everything lower case
            $seg = strtolower($seg);

            // Add it to cleaned segments if it's not empty
            if( isset($seg) && $seg != '' )
                $cleaned_segs[] = $seg;
        }


        // Set our cleaned segs
        $this->segments = $cleaned_segs;
    }


}




// -------------- PAGE-ACCESSIBLE FUNCIONS --------------


/*
 *  Outputs the html of the header from the template
 */
function show_header(){
    $st = new Statica();
    $st->show_page(NULL, 'header');
}


/*
 *  Outputs the html of the footer from the template
 */
function show_footer(){
    $st = new Statica();
    $st->show_page(NULL, 'footer');
}


/*
 *  Lists all of the pages for this website  
 */
function list_pages( $pages = NULL ){
    if( $data === NULL ){
        $st = new Statica();
        $pages = $st->return_pages(NULL, FALSE, FALSE);
    }

    $output = '';
    foreach( $pages as $key => $page ){
        // This is a regular page
        if( is_object($page) ){
            $output .= '<li><a href="'.$page->permalink.'">' .$page->title. '</a></li>';

        // This is a group with nested pages
        } else {
            $parent_data = $page['parent']; unset($page['parent']);
            $subs = current($page);

            $output .= '
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">'.$parent_data['title'].' <b class="caret"></b></a>
                <ul class="dropdown-menu">';
                    
                    foreach( $subs as $key => $sub ){
                        $output .= '<li><a href="'.$sub->permalink.'">'.$sub->title.'</a></li>';
                    }

                    $output .= '
                </ul>
            </li>
            ';
        }
    }

    echo $output;
}


/*
 *  Because it's easier with this  
 */
function print_var( $data ){
    echo '<pre>';
    print_r( $data );
    echo '</pre>';
}
