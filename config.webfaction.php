<?php

$config['website_name'] = "Statica";
$config['website_desc'] = "A static website CMS";

// The url to the main site
// NOTE: Don't forget the trailing slash
$config['base_url'] = "http://localhost/_personal/statica/";

// The url of blog posts, so we know that it's not a page
// Example: www.website.com/blog
$config['post_segment'] = 'blog';

// The default page to show when there are not segments
$config['default_page'] = 'home';

// The location of the website's content folder
// NOTE: Don't forget the trailing slash
$config['content_root'] = '_content/';

// The location of the assets within the $content_root
$config['media_root'] = $config['base_url'] . $config['content_root'] . '_assets/';

// The file extention given to all of your
// template files
// NOTE: Include the dot before the extension
$config['content_ext'] = '.md';

// The location of the website's template
// NOTE: Don't forget the trailing slash
$config['template_root'] = 'template/';

// The files within the content directory to display
// when we come across an error
$config['error_contents']['404'] = '404'; 
